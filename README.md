# Jake’s Delicious CAD Template

[![Screenshot of QCAD template](https://res.cloudinary.com/growdigital/image/upload/w_420/v1707654636/240211-cad-template-screenshot.png)](https://res.cloudinary.com/growdigital/image/upload/v1707654636/240211-cad-template-screenshot.png)  
_Click for hi-res_

CAD template file with nicely coloured layers.

I use [QCAD](https://qcad.org/) which is brilliant software. I made a [CAD for Gardeners](https://store.natureworks.org.uk/l/cad) class, showing you the basics.

Specifically for use with my template project [grwd.uk/template](https://grwd.uk/template)
